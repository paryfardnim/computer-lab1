module alu(num_1, num_2, control, output, zero)
    input[63:0] num_1, num_2;
    input[3:0] control;
    output[63:0] output;
    output zero;
    
   
    always@(num_1 or num_2 , control)
    begin
        ## control state check opertaions
        case(control)
            4'b0110 : output <= num_1 - num_2;
            4'b0010 : output <= num_1 + num_2;
            4'b0000 : output <= num_1 & num_2;
            4'b0001 : output <= num_1 | num_2;
        endcase
        zero = (output == 0) ? 1 : 0;
    end
endmodule
