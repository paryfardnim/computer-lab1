module registers(Read_register_1 , Read_register_2, write_status, Write_register, clk, 
begin
    Read_data_1, Read_data_2):

    input[4:0] Read_register_1,Read_register_2,Write_register;
    input clk;

    output reg[63:0] Read_data_1, Read_data_2;

    reg [63:0] data [31: 0];


    // read register one
    always@(Read_register_1)
    begin
        Read_data_1 <= (Read_register_1 ! = 0) ? data[Read_register_1] : 0
    end

    // read register two
    always@(Read_register_2)
    begin
        Read_data_2 <= (Read_register_2 ! = 0) ? data[Read_register_2] : 0
    end

    // if write_status(RegWrite) is 1 then write in data register
    always@(clk)
    begin
        (write_status == 2'b1)  data[Write_register] <= write_data
    end 
end