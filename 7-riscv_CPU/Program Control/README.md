# Control in RISC-V

## Main Control Unit

![alt](src/main_control_unit.jpg)

## Control Unit Truth Table

![alt](src/control_truth_table.png)