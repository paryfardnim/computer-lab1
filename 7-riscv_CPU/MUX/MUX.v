module multiplexer (I0,I1 , sel, out)

    parameter size = 64;
    input[size-1:0] I0,I1;
    input sel;
    output[size-1:0] out;
    
    always@(I0,I1 , sel)
    begin
        out < = (sel == 2'b1) ? I0 : I1;
    end
    
endmodule
