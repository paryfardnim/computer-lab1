`include "../multiplexer/MUX.v"

module control_tb;

    reg[63:0] I0 , I1;
    reg sel;
    wire[63:0] out; 

    multiplexer mux(I0, I1, sel, out);

initial begin

    // result is 00100010
    I0 = 30;
    I1 = 40;
    sel = 1;
    $display("control: %d", out ) // result is 30

    #40
    sel = 0;
    $display("control: %d", out ) // result is 40

end

always #10 clk = ~clk;

endmodule
