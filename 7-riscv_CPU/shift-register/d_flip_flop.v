// falling edge D flip flop
module d_flip_flop(input D,
                   output reg Q,
                   input clk,
                   input reset);
    always @(negedge clk)
    begin
        if (reset == 1'b1)
            Q <= 1'b0;
        else
            Q <= D;
    end
endmodule
