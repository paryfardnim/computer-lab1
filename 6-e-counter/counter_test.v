module counter_tb;



    reg clk; 
    reg reset;
    reg [1:0] y;
    reg UP;
    reg DN;
    reg A; 
    reg B; 
    reg E;


    cla_adder_4bit(.a(I) , .b(4'b0100) , .c_in(1'b0) , .s(A)) 

    counter usr(.UP(UP),.DN(DN),.A(flip_flop(y[0],A,clk,reset))
                                .B(flip_flop(y[1],B,clk,reset)),
                                .E(E))

initial begin

    // parallel shift register
    y = 2'b00;
    up = 2'b1;
    DN = 1'b0;
    E = 1'b1;

    y = 2'b10;
    up = 2'b0;
    DN = 1'b0;
    E = 1'b1;
    
    y = 2'b00;
    up = 2'b1;
    DN = 1'b0;
    E = 1'b0;
     
end

always #10 clk = ~clk;

end
