  module counter (
    UP,
    DN,
    A,  
    B,
    E,
    y,
    )
    
    wire [1:0]y;
    flip_flop flip_flpo_A(y[0],A,clk,reset);
    flip_flop flip_flop_B(y[1],B,clk,reset);
    input UP;
    input DN;

    
    always @(UP,DN)
    begin
        case (E): 
            1'b0:
                y <= {A,B}
            1'b1:
                case ({A,B})
                    : 
                    2'b00:
                        case ({UP,DN})
                            : 
                            2'b00:
                                y <= {A,B}
                            2'b01:
                                y <= {0,1}
                            2'b10:
                                y <= {1,1}
                        endcase
                    2'b01:
                        case ({UP,DN})
                            : 
                            2'b00:
                                y <= {A, B}
                            2'b01:
                                y <= {0,0}
                            2'b10:
                                y <= {1,0}
                        endcase
                    2'b11:
                        case ({UP,DN})
                            : 
                            2'b00:
                                y<= {A,B}
                            2'b01:
                                y<= {1,0}
                            2'b10:
                                y<= {1,1}
                        endcase
                    2'b10:
                        case ({UP,DN})
                            : 
                            2'b00:
                                y<={A,B}
                            2'b01:
                                y<={0,1}
                            2'b10:
                                y<={1,1}
                        endcase
                endcase
        endcase
    end
    
endmodule