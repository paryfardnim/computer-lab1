## nima parifard 972023007

#  logic

## Truth Table
| A | B | C | Out|
|---|---|---|-----|
| 0 | 0 | 0 | 0   |
| 0 | 0 | 1 | 0   |
| 0 | 1 | 1 | 1   |
| 1 | 0 | 0 | 0   |
| 1 | 0 | 1 | 1   |
| 1 | 1 | 0 | 1   |
| 1 | 1 | 1 | 1   |



## description
    A logic is a digital circuit whose output is equal to 1 if the majority of the inputs are 1’s. The output is 0 otherwise.



## Circuit Schematic
![alt](src/majority_circuits.SVG)


