## nima parifard 972023007
# Design proteus Example

## Step one : Truth Table
| A | B | C | D | F |
|---|---|---|---|---|
| 0 | 0 | 0 | 0 | 0 |
| 0 | 0 | 0 | 1 | 0 |
| 0 | 0 | 1 | 0 | 0 |
| 0 | 0 | 1 | 1 | 0 |
| 0 | 1 | 0 | 0 | 0 |
| 0 | 1 | 0 | 1 | 1 |
| 0 | 1 | 1 | 0 | 1 |
| 0 | 1 | 1 | 1 | 1 |
| 1 | 0 | 0 | 0 | 1 |
| 1 | 0 | 0 | 1 | 1 |
| 1 | 0 | 1 | 0 | 1 |
| 1 | 0 | 1 | 1 | 1 |
| 1 | 1 | 0 | 0 | 0 |
| 1 | 1 | 0 | 1 | 1 |
| 1 | 1 | 1 | 0 | 1 |
| 1 | 1 | 1 | 1 | 1 |

## Step two : Simply Truth Table
|        | c,d 00 | 01 | 11 | 10 |
|:------:|:------:|:--:|:--:|:--:|
| a,b 00 |    0   |  0 |  0 |  0 |
|   01   |    0   |  1 |  1 |  1 |
|   11   |    0   |  1 |  1 |  1 |
|   10   |    1   |  1 |  1 |  1 |

	f(a, b, c, d) = bd + bc + ab' // logic
    


## description

Design Circuit with this description:

Design a combinational circuit with four inputs — A, B, C, and D — and one output, F. F

is to be equal to 1 when A = 1, provided that B = 0, or when B = 1, provided that

either C or D is also equal to 1. Otherwise, the output is to be equal to 0

## Steps 

1. Obtain the truth table of the circuit.

2. Simplify the output function.

3. Draw the logic diagram of the circuit, using NAND gates with a minimum number of ICs.

4. Construct the circuit and test it for proper operation by verifying the given conditions.

