# Computer Architecture Laboratory

nima parifard - 972023007

**list of projects**

    task1:
        - cla adder 64 bit (project one)
            - implemnet full adder
            - implement cla adder 4,16,64 bit

    task2:  
            - universal shift register (project two)
                - implement right shift
                - implenment left shift
                - implemnet parallel load

    task3:
            - counter (project three)

    task4:
            - add ram.v ram_table.v
            - implement task 4

    task5: 
            - proteus
            - implement decoder
            - implement majority generator
            - implement logic
    task6: 
            - 16 azar
            - folder 6-e-counter
            - implement circiut counter
            - implement Enable(E)
            - e-counter state digram in src

     task7:
            - copy class adder and shift register and counter and ram to riscv cpu
            - add ALU , ALU-control and control
            - our risc cpu can do this task:
                 1- aithmatic operrator AND OR ADD SUBTRACT
                 2- branch and eqeual
                 3- load and store in memory
            

> check src folders for schematic circuits 