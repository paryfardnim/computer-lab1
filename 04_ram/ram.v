module ram (Data,
            address,
            data_out,
            );
    // set adreess size and data size variable    
    parameter address_size = 64;
    parameter data_size    = 32;
    
    // input adrress and output data pointer
    input [address_size-1:0] address;
    output [data_size-1:0] data_out;
    
    // 
    input reg [address_size-1:0] Data [data_size-1 : 0];  // define ram with 64 bit address & 32 bit data
    
    always @(*)
    begin
        data_out = Data[address]
    end
    
endmodule

// module ram(input clock, input [7:0] address, output reg [7:0] data);

// // Storage medium, using Verilog syntax for arrays
// reg [7:0] memory[255:0];

// always @(posedge clock)
//     data <= memory[address];

// endmodule