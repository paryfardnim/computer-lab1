module usv_tb;



    reg[3:0] I;
    reg clk; reg reset;
    reg [1:0] s;
    reg SIFSR; 
    reg SIFSL; 
    wire [3:0]A; 

    universal_shift_register usr(.I(I), .clk(clk), .reset(reset), .s(s), .SIFSR(SIFSR), .SIFSL(SIFSL));


initial begin
    
    // shift right test
    I     = 4'b0000;
    clk   = 1'b1;
    reset = 1'b1;
    SIFSR = 1'b1;
    SIFSL = 1'b0;
    s     = 2'b01;
    
    // shift left test
    #40;
    reset = 1'b1;
    SIFSR = 1'b0;
    SIFSL = 1'b1;
    s     = 2'b10;
    
    // parallel shift register
    #40;
    SIFSL = 1'b0;
    reset = 1'b1;
    I = 4'b0101;
    s     = 2'b11;
    
end

always #10 clk = ~clk;

end
