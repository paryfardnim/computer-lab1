`include "../01_cla_adder/cla_adder_4bit.v"
`include "../02_universal-shift-register/universal_shift_register.v"

module counter (
    I,
    clk,
    reset,
    s,
    SIFSR,  // serial input for shift right
    SIFSL
    )
    
    input [3:0]I;
    input [1:0]s;
    input clk;
    input reset;
    input SIFSR;
    input SIFSL;
    wire [3:0]A;
    
    universal_shift_register(A, I, clk, reset, s, SIFSR, SIFSL) // universal shift register
    cla_adder_4bit(I , 4'b0100 , 1'b0 , A) // adder to plus 4 to shift register
    
endmodule
