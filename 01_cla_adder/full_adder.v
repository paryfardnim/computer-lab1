// test case a = 1'b1 - b = 1'b1 - s = 1'b0
module full_adder(input a,
                  input b,
                  input c_in,
                  output s
                  );
    
    assign s = a ^ b ^ c_in;                //S = A ? B ? Cin
endmodule