// test case a = 4'b0100 , b = 4'b0101 , c_out = 1'b0
module cla_adder_4bit(input [3:0] a,
                      input [3:0] b,
                      input c_in,
                      output [3:0] s,
                      output c_out,
                      output pg,
                      output gg);

wire [3:0] p = a ^ b; // either a|b is work
wire [3:0] g = a&b;
wire [3:0] c ;


// find carry out for all full adders
assign c[0]  = c_in;
assign c[1]  = g[0] | p[0]&c[0];
assign c[2]  = g[1] | g[0]&p[1] | c[0]&p[0]&p[1];
assign c[3]  = g[2] | g[1]&p[2] | g[0]&p[1]&p[2] | c[0]&p[0]&p[1]&p[2];
assign c_out = g[3] | g[2]&p[3] | g[1]&p[2]&p[3] | g[0]&p[1]&p[2]&p[3]|c[0]&p[0]&p[1]&p[2]&p[3];

// full adders for 4 bit cla
full_adder full_adder_0(a[0] , b[0] , c[0] , s[0]);
full_adder full_adder_1(a[1] , b[1] , c[1] , s[1]);
full_adder full_adder_2(a[2] , b[2] , c[2] , s[2]);
full_adder full_adder_3(a[3] , b[3] , c[3] , s[3]);

// find pg & gg for bigger circuit
cla cla_0(c_in, p, g, pg, gg, c_out)
endmodule
